package com.VredLHom.testTasks.CleverPumpkin.Tools;


import android.util.Log;
import com.VredLHom.testTasks.CleverPumpkin.Trip.TakeOff_Landing;
import com.VredLHom.testTasks.CleverPumpkin.Trip.Trip;
import com.VredLHom.testTasks.CleverPumpkin.Trip.TripFlight;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Vred.L.Hom on 02.08.2014.
 */
public class XMLParser {

    public Trip getSpecificTrip(InputStreamReader isr) throws XmlPullParserException, IOException {

        Trip trip = null;

        try {

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(isr);
            int eventType = parser.nextTag();
            if (eventType == XmlPullParser.START_TAG
                    && parser.getName().equals("result")) {
                TakeOff_Landing takeOff = null;
                TakeOff_Landing landing = null;
                TripFlight flight = null;
                String[] duration = null;
                double price = 0;
                while (!(eventType == XmlPullParser.END_TAG && parser.getName().equals("result"))) {
                    eventType = parser.nextTag();
                    if (eventType == XmlPullParser.START_TAG && parser.getName().equals("trip")) {

                        duration = parser.getAttributeValue(null, "duration").split(":");
                        eventType = parser.nextTag();
                        String description = null;
                        String photoURL = null;

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("takeoff")) {
                            String[] date = parser.getAttributeValue(null, "date").split("-");
                            String[] time = parser.getAttributeValue(null, "time").split(":");
                            String city = parser.getAttributeValue(null, "city");
                            takeOff = new TakeOff_Landing(date, time, city);
                            parser.nextTag();
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("landing")) {
                            String[] date = parser.getAttributeValue(null, "date").split("-");
                            String[] time = parser.getAttributeValue(null, "time").split(":");
                            String city = parser.getAttributeValue(null, "city");
                            landing = new TakeOff_Landing(date, time, city);
                            parser.nextTag();
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("flight")) {
                            String carrier = parser.getAttributeValue(null, "carrier");
                            int number = Integer.valueOf(parser.getAttributeValue(null, "number"));
                            int eq = Integer.valueOf(parser.getAttributeValue(null, "eq"));
                            flight = new TripFlight(carrier, number, eq);
                            parser.nextTag();
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("price")) {
                            String str = parser.nextText();
                            price = Double.valueOf(str);
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("description")) {
                            description = parser.nextText();
                            Log.i("Photo", "Я зашёл в описание!");
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("photo")) {
                            Log.i("Photo", "Я зашёл в фото1!");
                            photoURL = parser.getAttributeValue(null, "src");
                            Log.i("Photo", "Я зашёл в фото!");
                            Log.i("Photo", photoURL);
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.END_TAG && parser.getName().equals("photo")) {
                            Log.i("Photo", "КОНЕЦ!");
                            trip = new Trip(duration, takeOff, landing, flight, price);
                            trip.setDescription(description);
                            trip.setPhotoURL(photoURL);
                            eventType = parser.nextTag();
                        }
                    }
                }

            }


        }catch (XmlPullParserException e) {
            Log.i("INFO2", "z nen");
            e.printStackTrace();
        } catch (IOException e) {
            Log.i("INFO3", "я тут");
            e.printStackTrace();
        }

        return trip;
    }

    public ArrayList<Trip> getTrips(InputStreamReader isr) throws XmlPullParserException, IOException {
        ArrayList<Trip> trips = new ArrayList<Trip>();


        try {

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(isr);
            trips = new ArrayList<Trip>();
            int eventType = parser.nextTag();
            if (eventType == XmlPullParser.START_TAG
                    && parser.getName().equals("result")) {
                Trip basicTrip;
                TakeOff_Landing takeOff = null;
                TakeOff_Landing landing = null;
                TripFlight flight = null;
                String[] duration = null;
                double price = 0;
                while (!(eventType == XmlPullParser.END_TAG && parser.getName().equals("result"))) {
                    eventType = parser.nextTag();
                    if (eventType == XmlPullParser.START_TAG && parser.getName().equals("trip")) {

                        duration = parser.getAttributeValue(null, "duration").split(":");
                        eventType = parser.nextTag();

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("takeoff")) {
                            String[] date = parser.getAttributeValue(null, "date").split("-");
                            String[] time = parser.getAttributeValue(null, "time").split(":");
                            String city = parser.getAttributeValue(null, "city");
                            takeOff = new TakeOff_Landing(date, time, city);
                            parser.nextTag();
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("landing")) {
                            String[] date = parser.getAttributeValue(null, "date").split("-");
                            String[] time = parser.getAttributeValue(null, "time").split(":");
                            String city = parser.getAttributeValue(null, "city");
                            landing = new TakeOff_Landing(date, time, city);
                            parser.nextTag();
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("flight")) {
                            String carrier = parser.getAttributeValue(null, "carrier");
                            int number = Integer.valueOf(parser.getAttributeValue(null, "number"));
                            int eq = Integer.valueOf(parser.getAttributeValue(null, "eq"));
                            flight = new TripFlight(carrier, number, eq);
                            parser.nextTag();
                            eventType = parser.nextTag();
                        }

                        if (eventType == XmlPullParser.START_TAG && parser.getName().equals("price")) {
                            String str = parser.nextText();
                            price = Double.valueOf(str);
                            eventType = parser.getEventType();
                        }

                        if (eventType == XmlPullParser.END_TAG && parser.getName().equals("price")) {
                            basicTrip = new Trip(duration, takeOff, landing, flight, price);
                            trips.add(basicTrip);
                            eventType = parser.nextTag();
                        }
                    }
                }

            }


        }catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return trips;
    }
}
