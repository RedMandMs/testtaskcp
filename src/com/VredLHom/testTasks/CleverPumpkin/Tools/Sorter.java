package com.VredLHom.testTasks.CleverPumpkin.Tools;

import android.text.format.Time;
import com.VredLHom.testTasks.CleverPumpkin.Trip.Trip;

import java.util.ArrayList;

/**
 * Created by Vred.L.Hom on 05.08.2014.
 */
public class Sorter {

    public ArrayList<Trip> sortPrice(ArrayList<Trip> tripList, boolean increase){

        for (int i = 0; i < tripList.size() - 1; i++) {
            for (int j = 0; j < tripList.size() - i - 1; j++) {
                if(increase) {
                    if (tripList.get(j).getPrice() > tripList.get(j + 1).getPrice()) {
                        Trip temp = tripList.get(j);
                        tripList.set(j, tripList.get(j + 1));
                        tripList.set(j + 1, temp);
                    }
                }else {
                    if (tripList.get(j).getPrice() < tripList.get(j + 1).getPrice()) {
                        Trip temp = tripList.get(j);
                        tripList.set(j, tripList.get(j + 1));
                        tripList.set(j + 1, temp);
                    }
                }
            }
        }

        return tripList;
    }

    public ArrayList<Trip> sortDuration(ArrayList<Trip> tripList, boolean increase){

        for (int i = 0; i < tripList.size() - 1; i++) {
            for (int j = 0; j < tripList.size() - i - 1; j++) {
                if(increase) {
                    if (Time.compare(tripList.get(j).getDuration(), tripList.get(j + 1).getDuration())>0) {
                        Trip temp = tripList.get(j);
                        tripList.set(j, tripList.get(j + 1));
                        tripList.set(j + 1, temp);
                    }
                }else {
                    if (Time.compare(tripList.get(j).getDuration(), tripList.get(j + 1).getDuration())<0) {
                        Trip temp = tripList.get(j);
                        tripList.set(j, tripList.get(j + 1));
                        tripList.set(j + 1, temp);
                    }
                }
            }
        }

        return tripList;
    }
}
