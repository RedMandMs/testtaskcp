package com.VredLHom.testTasks.CleverPumpkin.Trip;

import android.text.format.Time;
import android.util.Log;

/**
 * Created by Vred.L.Hom on 02.08.2014.
 */
public class TakeOff_Landing {

    private Time dateTime;
    private String city;

    public TakeOff_Landing(String[] date, String[] time, String city) {
        this.dateTime = new Time();
        this.dateTime.set(
                0,
                Integer.parseInt(time[1]),
                Integer.parseInt(time[0]),
                Integer.parseInt(date[2]),
                Integer.parseInt(date[1])-1,
                Integer.parseInt(date[0]));
        Log.i("INFO", dateTime.toString());
        this.city = city;
    }

    /*
    Getters and Setters
    */

    public String getCity() {
        return city;
    }

    public String getDateString(){
        return dateTime.format("%Y-%m-%d");
    }

    public String getTimeString(){
        return dateTime.format("%H:%M");
    }

}
