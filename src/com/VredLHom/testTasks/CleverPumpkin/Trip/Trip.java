package com.VredLHom.testTasks.CleverPumpkin.Trip;

import android.text.format.Time;

import java.util.HashMap;

/**
 * Created by Vred.L.Hom on 02.08.2014.
 */
public class Trip extends HashMap<String, String>{

    public static final String DURATION = "duration";
    public static final String DATETO = "dateTO";
    public static final String TIMETO = "timeTO";
    public static final String DATEL = "dateL";
    public static final String TIMEL = "timeL";
    public static final String CITYTO = "cityTO";
    public static final String CITYL = "cityL";
    public static final String CARRIER = "carrier";
    public static final String NUMBER = "number";
    public static final String EQ = "eq";
    public static final String PRICE = "price";
    public static final String DESCRIPTION = "description";
    public static final String PHOTOURL = "photo";


    private Time duration;
    private TakeOff_Landing takeOff;
    private TakeOff_Landing landing;
    private TripFlight flight;
    private double price;
    private String description;
    private String photoURL;


    public Trip(String[] duration, TakeOff_Landing takeOff, TakeOff_Landing landing, TripFlight flight, double price) {
        super();
        this.duration = new Time();
        this.duration.set(0, Integer.valueOf(duration[1]), Integer.valueOf(duration[0]), 0, 0, 0);
        super.put(DURATION, getDurationString());
        this.takeOff = takeOff;
        super.put(DATETO, takeOff.getDateString());
        super.put(TIMETO, takeOff.getTimeString());
        super.put(CITYTO, takeOff.getCity());
        this.landing = landing;
        super.put(DATEL, landing.getDateString());
        super.put(TIMEL, landing.getTimeString());
        super.put(CITYL, landing.getCity());
        this.flight = flight;
        super.put(CARRIER, flight.getCarrier());
        super.put(NUMBER, flight.getNumber());
        super.put(EQ, flight.getEq());
        this.price = price;
        super.put(PRICE, Double.toString(price));
    }

    /*
        Getters and Setters
    */

    public Time getDuration() {
        return duration;
    }

    public String getDurationString() {
        return duration.format("%H:%M");
    }

    public TakeOff_Landing getTakeOff() {
        return takeOff;
    }

    public TakeOff_Landing getLanding() {
        return landing;
    }

    public TripFlight getFlight() {
        return flight;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceString() {
        return Double.toString(price);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

}
