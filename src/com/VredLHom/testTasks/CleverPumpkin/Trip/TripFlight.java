package com.VredLHom.testTasks.CleverPumpkin.Trip;

/**
 * Created by Vred.L.Hom on 02.08.2014.
 */
public class TripFlight {


    private String carrier;
    private int number;
    private int eq;

    public TripFlight(String carrier, int number, int eq) {
        this.carrier = carrier;
        this.number = number;
        this.eq = eq;
    }

    /*
    Getters and Setters
     */

    public String getCarrier() {
        return carrier;
    }

    public String getNumber() {
        return Integer.toString(number);
    }

    public String getEq() {
        return Integer.toString(eq);
    }

}
