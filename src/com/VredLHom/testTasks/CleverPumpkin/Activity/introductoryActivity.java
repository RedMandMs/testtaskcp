package com.VredLHom.testTasks.CleverPumpkin.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import com.VredLHom.testTasks.CleverPumpkin.R;

public class IntroductoryActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    private AlertDialog.Builder builderAlterDialog;
    private Drawable introductoryPicture;
    private ImageView introductionIV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introductory_layout);

        introductoryPicture = getResources().getDrawable(R.drawable.go_fly);

        introductionIV = (ImageView) findViewById(R.id.introductoryPicture);
        Bitmap bitmapPicture = convertDrowInBitMap(getResources().getDrawable(R.drawable.go_fly));
        introductionIV.setImageBitmap(bitmapPicture);
    }

    public void onClickPicture(View view){
        setAlterDialog();
        builderAlterDialog.show();
    }

    private void setAlterDialog(){
        builderAlterDialog = new AlertDialog.Builder(this);
        builderAlterDialog.setTitle(getString(R.string.use_internet));  // заголовок
        builderAlterDialog.setMessage(getString(R.string.resolution_use_internet)); // сообщение
        builderAlterDialog.setPositiveButton(getString(R.string.yes), new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
                startActivity(intent);
            }
        });
        builderAlterDialog.setNegativeButton(getString(R.string.no), new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });
        builderAlterDialog.setCancelable(true);
        builderAlterDialog.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {

            }
        });
    }

    private Bitmap convertDrowInBitMap(Drawable drawable){
        // Конвертируем Drawable в Bitmap
        Bitmap bitmapPicture = BitmapFactory.decodeResource(getResources(), R.drawable.go_fly);
        return resizePicture(bitmapPicture);
    }

    private Bitmap resizePicture(Bitmap bitmapOriginal){
        //get the size of the screen and picture
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);
        int widthPicture = metricsB.widthPixels;
        int heightPicture = (bitmapOriginal.getHeight() * (widthPicture) / (bitmapOriginal.getWidth()));

        Bitmap finalBitmapImage = Bitmap.createScaledBitmap(bitmapOriginal, widthPicture,
                heightPicture, true);
        return finalBitmapImage;
    }

}
