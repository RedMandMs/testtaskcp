package com.VredLHom.testTasks.CleverPumpkin.Activity.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.VredLHom.testTasks.CleverPumpkin.R;

/**
 * Created by Vred.L.Hom on 04.08.2014.
 */
public class welcomeFragment extends Fragment {

    Activity parentActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        parentActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_fragment_layout, container, false);
    }
}
