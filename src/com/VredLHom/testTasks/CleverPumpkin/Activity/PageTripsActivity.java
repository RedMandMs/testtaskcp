package com.VredLHom.testTasks.CleverPumpkin.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.VredLHom.testTasks.CleverPumpkin.R;
import com.VredLHom.testTasks.CleverPumpkin.Tools.Sorter;
import com.VredLHom.testTasks.CleverPumpkin.Tools.XMLParser;
import com.VredLHom.testTasks.CleverPumpkin.Trip.Trip;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static android.view.MenuItem.OnMenuItemClickListener;

/**
 * Created by Vred.L.Hom on 10.08.2014.
 */
public class PageTripsActivity extends FragmentActivity {

    public static final int IDM_SORT_PRICE_INCREASE = 101;
    public static final int IDM_SORT_PRICE_DECREASE = 102;
    public static final int IDM_SORT_DURATION_INCREASE = 201;
    public static final int IDM_SORT_DURATION_DECREASE = 202;

    public static final int COLOR_ODD_SRING = 0xff696566;
    public static final int COLOR_EVEN_SRING = 0xffaeaaab;

    private ArrayList<Trip> tripArrayList;
    private TableLayout tableLayout;
    private LayoutInflater inflater;
    private DownloadSpecTrip downloadSpecTrip;
    private ProgressDialog progressDialog;

    public void onClickTrip(View view){
        showProgressDialog();
        downloadSpecTrip = new DownloadSpecTrip(view);
        downloadSpecTrip.execute();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.table_trips_layoute);
        tableLayout = (TableLayout) findViewById(R.id.TableTrips);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tripArrayList = RequestActivity.tripArrayList;

        addTrips();

    }

    private void showProgressDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.please_wait));

        progressDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.cansel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                downloadSpecTrip.cancel(true);
                closeProgressDialog();
            }

        });
    }

    private void closeProgressDialog() {
        progressDialog.cancel();
    }

    private void addTrips(){
        boolean odd = true;
        for (int i = 0; i < tripArrayList.size(); i++) {
            addRow(tripArrayList.get(i), odd);
            odd = !odd;
        }
    }

    private void addRow(Trip trip, boolean odd) {
        TableRow tableRow = (TableRow) inflater.inflate(R.layout.row_trip, null);

        if(odd){
            tableRow.setBackgroundColor(COLOR_ODD_SRING);
        }else{
            tableRow.setBackgroundColor(COLOR_EVEN_SRING);
        }

        TextView tv = (TextView) tableRow.getChildAt(0);
        tv.setText(trip.getTakeOff().getCity());
        tv = (TextView) tableRow.getChildAt(1);
        tv.setText(trip.getTakeOff().getDateString());
        tv = (TextView) tableRow.getChildAt(2);
        tv.setText(trip.getTakeOff().getTimeString());
        tv = (TextView) tableRow.getChildAt(3);
        tv.setText(trip.getDurationString());
        tv = (TextView) tableRow.getChildAt(4);
        tv.setText(trip.getLanding().getCity());
        tv = (TextView) tableRow.getChildAt(5);
        tv.setText(trip.getLanding().getDateString());
        tv = (TextView) tableRow.getChildAt(6);
        tv.setText(trip.getLanding().getTimeString());
        tv = (TextView) tableRow.getChildAt(7);
        tv.setText(trip.getFlight().getCarrier());
        tv = (TextView) tableRow.getChildAt(8);
        tv.setText(trip.getFlight().getNumber());
        tv = (TextView) tableRow.getChildAt(9);
        tv.setText(trip.getFlight().getEq());
        tv = (TextView) tableRow.getChildAt(10);
        tv.setText(trip.getPriceString());
        tableLayout.addView(tableRow);
    }

    private void clearTrips(){
        for (int i = 0; i < tripArrayList.size(); i++) {
            removeRow();
        }
    }

    private void removeRow() {
        TableRow tableRow = (TableRow) tableLayout.findViewById(R.id.tripRow);
        tableLayout.removeView(tableRow);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SubMenu sortMenu = menu.addSubMenu(getResources().getString(R.string.sort));

        sortMenu.add(Menu.NONE, IDM_SORT_PRICE_INCREASE, Menu.NONE, getResources().getString(R.string.sort_price_increase));
        sortMenu.add(Menu.NONE, IDM_SORT_PRICE_DECREASE, Menu.NONE, getResources().getString(R.string.sort_price_decrease));
        sortMenu.add(Menu.NONE, IDM_SORT_DURATION_INCREASE, Menu.NONE, getResources().getString(R.string.sort_duration_increase));
        sortMenu.add(Menu.NONE, IDM_SORT_DURATION_DECREASE, Menu.NONE, getResources().getString(R.string.sort_duration_decrease));

        setActionListnerMenu(menu);
        return true;
    }

    private void setActionListnerMenu(Menu menu){

        MenuItem sortPriceIncrease = menu.findItem(IDM_SORT_PRICE_INCREASE);
        sortPriceIncrease.setOnMenuItemClickListener(new MenuItemListner());

        MenuItem sortPriceDecrease = menu.findItem(IDM_SORT_PRICE_DECREASE);
        sortPriceDecrease.setOnMenuItemClickListener(new MenuItemListner());

        MenuItem sortDurationIncrease = menu.findItem(IDM_SORT_DURATION_INCREASE);
        sortDurationIncrease.setOnMenuItemClickListener(new MenuItemListner());

        MenuItem sortDurationDecrease = menu.findItem(IDM_SORT_DURATION_DECREASE);
        sortDurationDecrease.setOnMenuItemClickListener(new MenuItemListner());
    }

    private void sortPrice(boolean increase){
        Sorter sorter = new Sorter();
        tripArrayList = sorter.sortPrice(tripArrayList, increase);
    }

    private void sortDuration(boolean increase){
        Sorter sorter = new Sorter();
        tripArrayList = sorter.sortDuration(tripArrayList, increase);
    }

    private class DownloadSpecTrip extends AsyncTask<URL, Void, Void> {

        private View clickView;
        private Trip specTrip;
        private boolean isDownloaded;
        private AlertDialog alertDialogError;

        public DownloadSpecTrip(View clickView){
            this.clickView = clickView;
        }

        @Override
        protected Void doInBackground(URL... params) {

            isDownloaded = downloadSpecTrip(clickView);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.cancel();
            if(isDownloaded) {
                Intent intent = fillIntent(specTrip);
                startActivity(intent);
            }else{
                showMessageError();
            }
        }

        private boolean downloadSpecTrip(View view){
            try{
                TableRow trip = (TableRow) view;
                TextView numberTripTV = (TextView) trip.getChildAt(8);
                String numberTrip = numberTripTV.getText().toString();
                URL urlTrip = new URL(getResources().getString(R.string.trip_url) + numberTrip + getString(R.string.xml_format));


                URLConnection connection;
                connection = urlTrip.openConnection();
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                int responseCode = httpConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    InputStream inputStream = httpConnection.getInputStream();

                    InputStreamReader reader = new InputStreamReader(inputStream);

                    XMLParser xmlParser = new XMLParser();

                    specTrip = xmlParser.getSpecificTrip(reader);

                }

                return true;

            } catch (IOException e) {
                return false;
            } catch (XmlPullParserException e) {
                return false;
            }
        }

        private Intent fillIntent(Trip trip){
            Intent intent = new Intent(getApplicationContext(), SpecificTripActivity.class);
            intent.putExtra(Trip.DURATION, trip.getDurationString());
            intent.putExtra(Trip.CITYTO, trip.getTakeOff().getCity());
            intent.putExtra(Trip.DATETO, trip.getTakeOff().getDateString());
            intent.putExtra(Trip.TIMETO, trip.getTakeOff().getTimeString());
            intent.putExtra(Trip.CITYL, trip.getLanding().getCity());
            intent.putExtra(Trip.DATEL, trip.getLanding().getDateString());
            intent.putExtra(Trip.TIMEL, trip.getLanding().getTimeString());
            intent.putExtra(Trip.CARRIER, trip.getFlight().getCarrier());
            intent.putExtra(Trip.NUMBER, trip.getFlight().getNumber());
            intent.putExtra(Trip.EQ, trip.getFlight().getEq());
            intent.putExtra(Trip.PRICE, trip.getPriceString());
            intent.putExtra(Trip.DESCRIPTION, trip.getDescription());
            intent.putExtra(Trip.PHOTOURL, trip.getPhotoURL());

            return intent;
        }

        private void showMessageError() {
            alertDialogError = createMessageError();
            alertDialogError.show();
        }

        private AlertDialog createMessageError(){
            AlertDialog.Builder builderAD = new AlertDialog.Builder(PageTripsActivity.this);
            builderAD.setTitle(getResources().getString(R.string.error))
                    .setMessage(getResources().getString(R.string.no_internet))
                    .setNegativeButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            return builderAD.create();
        }

    }


    private class MenuItemListner implements OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if(item.getTitle().equals(getResources().getString(R.string.sort_price_increase))) {
                sortPrice(true);
                clearTrips();
                addTrips();
            }
            if(item.getTitle().equals(getResources().getString(R.string.sort_price_decrease))) {
                sortPrice(false);
                clearTrips();
                addTrips();
            }
            if(item.getTitle().equals(getResources().getString(R.string.sort_duration_increase))) {
                sortDuration(true);
                clearTrips();
                addTrips();
            }
            if(item.getTitle().equals(getResources().getString(R.string.sort_duration_decrease))) {
                sortDuration(false);
                clearTrips();
                addTrips();
            }
            return false;
        }
    }

}
