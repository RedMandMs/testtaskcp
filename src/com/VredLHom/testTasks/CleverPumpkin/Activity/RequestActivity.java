package com.VredLHom.testTasks.CleverPumpkin.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.VredLHom.testTasks.CleverPumpkin.R;
import com.VredLHom.testTasks.CleverPumpkin.Tools.XMLParser;
import com.VredLHom.testTasks.CleverPumpkin.Trip.Trip;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by Vred.L.Hom on 04.08.2014.
 */
public class RequestActivity extends FragmentActivity {

    protected static ArrayList<Trip> tripArrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_layout);

    }

    public void clickFlyBtn(View view){
        showProgressDialog();
        showTrips();
    }

    private ProgressDialog progressDialog;
    private DownloadAllTrip downloadAllTrip;

    private void showProgressDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.please_wait));

        progressDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.cansel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                downloadAllTrip.cancel(true);
                closeProgressDialog();
            }

        });

        progressDialog.show();
    }

    private void closeProgressDialog() {
        progressDialog.cancel();
    }

    private void showTrips(){

        downloadAllTrip = new DownloadAllTrip();
        downloadAllTrip.execute();

    }

    private class DownloadAllTrip extends AsyncTask<Void, Integer, Boolean>{

        private AlertDialog alertDialogError;
        private Boolean isDownloaded;

        @Override
        protected Boolean doInBackground(Void... params) {
            isDownloaded = downloadTrips();
            return isDownloaded;
        }

        @Override
        protected void onPostExecute(Boolean isDownloaded) {
            super.onPostExecute(isDownloaded);
            progressDialog.cancel();
            if(isDownloaded) {
                Intent intent = new Intent(getApplicationContext(), PageTripsActivity.class);
                startActivity(intent);
            }else{
                showMessageError();
            }
        }

        private boolean downloadTrips(){
            URL url;

            try {
                url = new URL(getResources().getString(R.string.main_url));
                URLConnection connection;
                connection = url.openConnection();
                HttpURLConnection httpConnection = (HttpURLConnection) connection;

                int responseCode = httpConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {

                    InputStream inputStream = httpConnection.getInputStream();

                    InputStreamReader reader = new InputStreamReader(inputStream);

                    XMLParser xmlParser = new XMLParser();

                    tripArrayList = xmlParser.getTrips(reader);

                }
                return true;
            } catch (MalformedURLException e) {
                return false;
            } catch (XmlPullParserException e) {
                return false;
            } catch (IOException e) {
                return false;
            }
        }

        private void showMessageError() {
            alertDialogError = createMessageError();
            alertDialogError.show();
        }

        private AlertDialog createMessageError(){
            AlertDialog.Builder builderAD = new AlertDialog.Builder(RequestActivity.this);
            builderAD.setTitle(getResources().getString(R.string.error))
                    .setMessage(getResources().getString(R.string.no_internet))
                    .setNegativeButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            return builderAD.create();
        }
    }
}