package com.VredLHom.testTasks.CleverPumpkin.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.ImageView;
import android.widget.TextView;
import com.VredLHom.testTasks.CleverPumpkin.R;
import com.VredLHom.testTasks.CleverPumpkin.Trip.Trip;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Vred.L.Hom on 13.08.2014.
 */
public class SpecificTripActivity extends FragmentActivity {

    private TextView carrierTV;
    private TextView numberTripTV;
    private TextView eqTV;

    private TextView cityDepTV;
    private TextView durationTV;
    private TextView cityArrivTV;

    private TextView dateTimeDepTV;
    private TextView dateTimeArrivTV;

    private ImageView imageIV;

    private TextView descriptionTV;
    private TextView priceTV;

    private String urlImage;

    private ProgressDialog progressDialog;
    private DownloadPhotoTrip downloadPhotoTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spec_trip_layoute);
        showProgressDialog();
        loadingData();
    }

    private void showProgressDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.please_wait));

        progressDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.cansel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                downloadPhotoTrip.cancel(true);
                closeProgressDialog();
            }

        });

        progressDialog.show();
    }

    private void closeProgressDialog(){
        progressDialog.cancel();
    }

    private void loadingData(){
        initView();
        setText();

        downloadPhotoTrip = new DownloadPhotoTrip();
        downloadPhotoTrip.execute();
    }

    private void initView(){
        carrierTV = (TextView) findViewById(R.id.specCarrier);
        numberTripTV = (TextView) findViewById(R.id.specNumber);
        eqTV = (TextView) findViewById(R.id.specEq);

        cityDepTV = (TextView) findViewById(R.id.specCityDep);
        durationTV = (TextView) findViewById(R.id.specDuration);
        cityArrivTV = (TextView) findViewById(R.id.specCityArriv);

        dateTimeDepTV = (TextView) findViewById(R.id.specDateTimeDep);
        dateTimeArrivTV = (TextView) findViewById(R.id.specDateTimeArriv);

        imageIV = (ImageView) findViewById(R.id.specImage);

        descriptionTV = (TextView) findViewById(R.id.specDescription);

        priceTV = (TextView) findViewById(R.id.specPrice);
    }

    private void setText(){

        carrierTV.setText(getIntent().getStringExtra(Trip.CARRIER));
        numberTripTV.setText(getString(R.string.number_trip) + getIntent().getStringExtra(Trip.NUMBER));
        eqTV.setText(getString(R.string.plan_eq) + getIntent().getStringExtra(Trip.EQ));

        cityDepTV.setText(getString(R.string.departure_from) + "\n" + getIntent().getStringExtra(Trip.CITYTO));
        durationTV.setText(getString(R.string.time_in_road) + "\n" + getIntent().getStringExtra(Trip.DURATION));
        cityArrivTV.setText(getString(R.string.arrival_in) + "\n" + getIntent().getStringExtra(Trip.CITYL));

        dateTimeDepTV.setText(getIntent().getStringExtra(Trip.DATETO)+"\n" + getIntent().getStringExtra(Trip.TIMETO));
        dateTimeArrivTV.setText(getIntent().getStringExtra(Trip.DATEL)+"\n" + getIntent().getStringExtra(Trip.TIMEL));

        urlImage = getIntent().getStringExtra(Trip.PHOTOURL);


        descriptionTV.setText(getIntent().getStringExtra(Trip.DESCRIPTION));
        priceTV.setText(getString(R.string.price) + getIntent().getStringExtra(Trip.PRICE) + getString(R.string.rub));

    }

    private class DownloadPhotoTrip extends AsyncTask<URL, Void, Bitmap> {

        private static final double PROPORTION_IMAGE = 0.8;

        private AlertDialog alertDialogError;
        private Bitmap imagePlanBM;
        private Boolean isDownloaded;

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            pastePicture(bitmap);
            closeProgressDialog();
            if(!(isDownloaded)){
                showMessageError();
            }
        }

        @Override
        protected Bitmap doInBackground(URL... urlPhoto) {

            imagePlanBM = null;

            // Загружаем картинку из интернета
            urlImage = getIntent().getStringExtra(Trip.PHOTOURL);
                URL myURL;

                try {
                    myURL = new URL(urlImage);
                    imagePlanBM = BitmapFactory.decodeStream(myURL
                            .openConnection().getInputStream());
                    isDownloaded = true;
                } catch (IOException e) {
                    imagePlanBM = BitmapFactory.decodeResource(
                            getApplicationContext().getResources(),
                            R.drawable.no_picture);
                    isDownloaded = false;
                }
            return imagePlanBM;
        }

        private void pastePicture(Bitmap bitmap) {

            //resize the image
            Bitmap finalBitmapImage = getFinalBMImage(bitmap);

            //Paste image
            imageIV.setImageBitmap(finalBitmapImage);

            //Save result
            imagePlanBM = finalBitmapImage;
        }

        private Bitmap getFinalBMImage(Bitmap bitmapOriginal){

            //get the size of the screen and picture
            Display display = getWindowManager().getDefaultDisplay();
            DisplayMetrics metricsB = new DisplayMetrics();
            display.getMetrics(metricsB);
            int widthDisplay = metricsB.widthPixels;
            int widthPicture = (int) (widthDisplay * PROPORTION_IMAGE);
            int heightPicture = (bitmapOriginal.getHeight() * (widthPicture) / (bitmapOriginal.getWidth()));

            Bitmap finalBitmapImage = Bitmap.createScaledBitmap(bitmapOriginal, widthPicture,
                    heightPicture, true);
            return finalBitmapImage;
        }

        private void showMessageError() {
            alertDialogError = createMessageError();
            alertDialogError.show();
        }

        private AlertDialog createMessageError(){
            AlertDialog.Builder builderAD = new AlertDialog.Builder(SpecificTripActivity.this);
            builderAD.setTitle(getResources().getString(R.string.error))
                    .setMessage(getString(R.string.no_image))
                    .setNegativeButton(getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            return builderAD.create();
        }
    }


}
